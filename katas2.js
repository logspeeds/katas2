function add (num1, num2) {
    return (num1 + num2)
}

function multiply (num1, num2) {
    let acumulado = 0
    for (let a = 1; a <= num2; a++) {
        acumulado = acumulado + num1
    }
    return acumulado 
}

function power (num1, num2) {
    let acumulado = num1
    for (let a = 1; a <= num2 - 1; a++) {
        acumulado = multiply (acumulado, num1)
    }
    return acumulado
}

function factorial (num1) {
    let MenosFatorial = num1 - 1
    let acumulado = num1
    for (let a = num1; a > 1; a = a - 1) { 
        acumulado = multiply (acumulado, MenosFatorial)
        MenosFatorial = MenosFatorial - 1
    }
    return acumulado
}
